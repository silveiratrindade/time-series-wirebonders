import datetime
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters
import src.packages.db.db_connect as db

# register_matplotlib_converters()

def get_dataset_start(col_timestampdata="timestampdata", col_x_axis="x_axis", col_y_axis="y_axis", \
        col_z_axis="z_axis", table_name="public.datalog", qtd_rows=100):

    sql_query = """SELECT {timestamp} AT TIME ZONE 'America/Sao_Paulo' AS timestampdata, {x_axis}, {y_axis}, {z_axis} \
    FROM {table} LIMIT {rows}""".format(timestamp=col_timestampdata, x_axis=col_x_axis, y_axis=col_y_axis, \
        z_axis=col_z_axis, table=table_name, rows=qtd_rows)

    conn = db.connect()
    dataset = pd.read_sql_query(sql_query, conn)

    return dataset

def get_dataset_from_db(start_year=2019, start_month=5, start_day=14, start_hour=12, start_min=44, start_sec=40, start_microsec=0,\
    interval_sec=0, col_timestampdata="timestampdata", col_x_axis="x_axis", col_y_axis="y_axis", \
        col_z_axis="z_axis", table_name="public.datalog"):
    '''This function only works if you dataset will have 4 columns in database. All parameters are optionals.\n
    Define timestamp start data using year, month, day, hour, min, sec, milisec, acordind data stored in DB. \n
    Define the interval parameter in seconds for window select. \n
    Change col_timestampdata, col_x_axis, col_y_axis, col_z_axis and table if necessary.\n
    The return are dataset, timestamp_start and timestamp_end\n\n

    Example:
    
    dataset_raw, timestamp_start, timestamp_end = get_dataset_from_db(start_min=54, start_sec=41, interval_sec=interval_sec)\n\n
        
    If you don't want timestamp returns, uset '_' in each position: \n\n

    dataset_raw, _, _ = get_dataset_from_db(start_min=54, start_sec=41, interval_sec=interval_sec)
    
    '''

    timestamp_start = datetime.datetime(start_year, start_month, start_day, start_hour, start_min, \
        start_sec, start_microsec) # year, month, day, hour, min, sec...
    timestamp_end = timestamp_start + datetime.timedelta(seconds=interval_sec)

    timestamp_start_str = '\'' + str(timestamp_start) + '\''
    timestamp_end_str = '\'' + str(timestamp_end) + '\''

    sql_query = """
    SELECT {timestamp} AT TIME ZONE 'America/Sao_Paulo' AS timestampdata, {x_axis}, {y_axis}, {z_axis} \
    FROM {table} WHERE timestampdata >= {start_time} AND timestampdata < {end_time} \
    ORDER BY timestampdata ASC""".format(timestamp=col_timestampdata, x_axis=col_x_axis, y_axis=col_y_axis, \
        z_axis=col_z_axis, table=table_name, start_time=timestamp_start_str, end_time=timestamp_end_str)

    conn = db.connect()
    dataset = pd.read_sql_query(sql_query, conn)
    
    return dataset, timestamp_start, timestamp_end

def get_dataset_from_csv(csv_file):

    # csv_file = r'C:\_Trindade\Git\time-series-csv\data\csv\split_1M\output_1.csv'
    # dataset = pd.read_csv(csv_file, iterator=True, chunksize=chunkrows) 
    dataset=[]

    try:
        dataset = pd.read_csv(csv_file, delimiter = ',', names=["timestampdata", "x_axis", "y_axis", "z_axis"]) 
    except:
        print('Arquivo não encontrado: ' + csv_file)    

    return dataset