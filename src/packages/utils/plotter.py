# from pylab import errorbar, plot
import matplotlib.pyplot as plt

class DynamicPlotter(object):

    def __init__(self, opt, episodes):
        self.opt = opt
        self.xdata = []
        self.ydata = []
        #plt.show()
        self.axes = plt.gca()
        self.axes.set_xlim(0, episodes)
        #self.axes.set_ylim(self.opt*.95, 100)
        self.line_avg, = self.axes.plot(self.xdata, self.ydata, 'r-', label='Average Travel Time')
        self.line_opt, = self.axes.plot([x for x in xrange(episodes)], [self.opt for _ in xrange(episodes)], 'b--', label='System Optimum')
        plt.legend(loc='upper right')
        plt.title('Average travel time along episodes', size=16)
        plt.xlabel('episode')
        plt.ylabel('average travel time')

    def update(self, x, y):
        #print '(%d,%d)' % (x,y)
        self.xdata.append(x)
        self.ydata.append(y)
        self.line_avg.set_xdata(self.xdata)
        self.line_avg.set_ydata(self.ydata)
        self.axes.set_ylim(self.opt*.95, max(self.ydata))
        plt.draw()
        plt.pause(1e-17)
        #time.sleep(0.1)

    def show_final(self):
        plt.show()