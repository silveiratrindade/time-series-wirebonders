import psycopg2
from sqlalchemy import create_engine

def connect():

    # Postgres username, password, and database name 
    POSTGRES_ADDRESS = '192.168.145.131' ## INSERT YOUR DB ADDRESS IF IT'S NOT ON PANOPLY 
    POSTGRES_PORT = '5432' ## PostgreSQL Default Port
    POSTGRES_USERNAME = 'postgres' ## CHANGE THIS TO YOUR PANOPLY/POSTGRES USERNAME 
    POSTGRES_PASSWORD = 'postgres' ## CHANGE THIS TO YOUR PANOPLY/POSTGRES PASSWORD
    POSTGRES_DBNAME = 'timeseries' ## CHANGE THIS TO YOUR DATABASE NAME 
    # A long string that contains the necessary Postgres login information 
    postgres_str = ('postgresql://{username}:{password}@{ipaddress}:{port}/{dbname}'
        .format(username=POSTGRES_USERNAME, 
        password=POSTGRES_PASSWORD, 
        ipaddress=POSTGRES_ADDRESS, 
        port=POSTGRES_PORT, 
        dbname=POSTGRES_DBNAME)) 
    # Create the connection 
    conn = create_engine(postgres_str)

    return conn