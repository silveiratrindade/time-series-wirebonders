import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt

from tslearn.preprocessing import TimeSeriesResampler, TimeSeriesScalerMinMax
# from tslearn.piecewise import PiecewiseAggregateApproximation, SymbolicAggregateApproximation
from tslearn.metrics import lb_envelope

import src.packages.db.db_connect as db
import src.packages.utils.dataset as ds

# Pandas dependency for matplotlib. Don't remove!
register_matplotlib_converters()

def Preprocessing(dataset_axes, dataset_timestamp, interval_sec=5,  n_segments=2000, n_layers=20, absolute=False, resample=False, \
    envelope=False, scale=False):

    # Transform to Series
    default_series = pd.Series(dataset_axes)
    series = pd.Series(dataset_axes, index=dataset_timestamp)

    # Transform to Absolute
    if absolute:
        series = series.abs()

    # Resample
    if resample:
        resample_size = n_segments * interval_sec
        default_series = pd.Series(TimeSeriesResampler(sz=resample_size).fit_transform(default_series).ravel())
        series = pd.Series(TimeSeriesResampler(sz=resample_size).fit_transform(series).ravel())

    # Envelope
    if envelope:
        env_low, env_up = lb_envelope(series, radius=200)
        series = pd.Series(env_up.ravel())

    # Scale
    # sax = SymbolicAggregateApproximation(n_segments=n_segments, alphabet_size_avg=n_sax_layers)

    if scale:

        scale_range = [0,1,2,3,4,5,6,7,8,9,10]

        min_scaler = min(scale_range)
        max_scaler = max(scale_range)

        scaler = TimeSeriesScalerMinMax(min=min_scaler, max=max_scaler)
    
        series = scaler.fit_transform(series)

    # series = pd.Series(sax.inverse_transform(sax.fit_transform(series))[0].ravel())

    return series, default_series

def Segmentation(series, scale_range):

    idx_list = list(series)
    segmentation = []

    segment = True

    for i in range(len(idx_list)):
            
        if idx_list[i] != scale_range[1]:
            # segmentation.append(i)	
            if not segment:
                segmentation.append(i)
                pass

            segment = True				
        else:
            segment = False

    return segmentation

def Plot(series, default_series, n_layers, scale_range, n_segments, min_scaler, max_scaler):

    figsize=(14, 6)

    plt.figure(figsize=figsize)
    plt.subplot(2, 1, 1)  # First, raw time series
    plt.plot(default_series.ravel(), "b-")
    # plt.vlines(segmentation, min(default_series), max(default_series), 'red', 'dashed', lw=1)

    plt.title("Raw time series")

    plt.subplot(2, 1, 2)  # Then SAX
    # plt.plot(series.ravel(), "b-", alpha=0.4)
    plt.plot(series, 'b-')
    plt.hlines(scale_range, 0, n_segments, 'gray', 'dashed', alpha=0.4)
    # plt.vlines(segmentation, min_scaler, max_scaler, 'r', 'dashed', lw=1)

    plt.title("Layers, %d symbols" % n_layers)

    plt.tight_layout()

    plt.show()


def main():

    axes = 'y_axis'
    timestamp = 'timestampdata'
    interval_sec = 5
    n_layers = 20
    n_segments = 2000

    # Get Dataset from DB
    dataset_raw, ts, te = ds.get_dataset_from_db(start_year=2018, start_month=10, start_day=28, start_hour=21, start_min=54, \
        start_sec=19, interval_sec=interval_sec)

    # dataset_raw = ds.get_dataset_from_csv(r'C:\_Trindade\Git\time-series-csv\data\csv\split_1M\output_1.csv')

    dataset_axes = dataset_raw[axes].values
    dataset_timestamp = dataset_raw[timestamp]


    series, default_series, min_scaler, max_scaler, scaler, scale_range = Preprocessing(dataset_axes, dataset_timestamp, interval_sec, n_layers, n_segments)

    # segmentation = Segmentation(series, scale_range[1])

    Plot(series, default_series, n_layers, scale_range, n_segments, min_scaler, max_scaler)


    


if __name__ == "__main__":
    main()