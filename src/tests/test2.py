import numpy as np
import pandas as pd
import scipy.signal as sig
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from tslearn.preprocessing import TimeSeriesResampler, TimeSeriesScalerMinMax
from tslearn.piecewise import PiecewiseAggregateApproximation, SymbolicAggregateApproximation
from tslearn.metrics import lb_envelope, dtw

import src.packages.db.db_connect as db
import src.packages.utils.dataset as ds

# %matplotlib notebook

register_matplotlib_converters()

# figsize=(14, 6)

# Utils
def get_pattern(dataset, idx_start, idx_end):
    pattern = []

    for idx in range(idx_start, idx_end):

        pattern.append(dataset[idx])

    return pattern


# Dataset
axes = 'y_axis'
timestamp = 'timestampdata'

interval_sec = 25
window = 2000
total_segment_size = 2000 * interval_sec

dataset_raw, ts, te = ds.get_dataset_from_db(start_min=54, start_sec=40, interval_sec=interval_sec)

dataset_axes = dataset_raw[axes].values
dataset_timestamp = dataset_raw[timestamp]

# Series
default_series = pd.Series(dataset_axes)
series = pd.Series(dataset_axes)

# # Absolute
# series = series.abs()

# Resample
resample_size = total_segment_size
default_series = pd.Series(TimeSeriesResampler(sz=resample_size).fit_transform(default_series).ravel())
series = pd.Series(TimeSeriesResampler(sz=resample_size).fit_transform(series).ravel())



# Patterns
data_window = []
default_patterns = []
patterns_A = []
patterns_B = []
dtw_A = []
dtw_B = []
dtw_AB = []

default_patterns.append(get_pattern(default_series, 125, 287))
default_patterns.append(get_pattern(default_series, 287, 478))

start_idx = 0
# len_series = len(default_series)

# CONTINUAR DAQUI...

# for sim in range(int(len(default_series)/window)):
while start_idx < len(default_series) - 1336:
# while True:

    # window_series = series

    # if start_idx >= len(default_series):
    #     break

    for s in range(start_idx, window + start_idx):

        data_window.append(series[s])


    window_series = pd.Series(data_window)

    # Absolute
    window_series = window_series.abs()    

    # Envelope
    env_low, env_up = lb_envelope(window_series, radius=35)
    window_series = pd.Series(env_up.ravel())

    # Scale to Sax
    n_paa_segments = window
    n_sax_symbols = 20
    sax = SymbolicAggregateApproximation(n_segments=n_paa_segments, alphabet_size_avg=n_sax_symbols)
    min_scaler = min(sax.breakpoints_avg_middle_) + .2
    max_scaler = max(sax.breakpoints_avg_middle_)
    scaler = TimeSeriesScalerMinMax(min=min_scaler, max=max_scaler)
    window_series = scaler.fit_transform(window_series)
    sax_dataset_inv = sax.inverse_transform(sax.fit_transform(window_series))

    # Index Segmentation
    sax_series = pd.Series(sax_dataset_inv.ravel())
    idx_data = []
    idx_segment = True
    idx = 0

    for idx in range(len(sax_series)):
            
        if sax_series[idx] != sax.breakpoints_avg_middle_[0]:
            # idx_data.append(i)	
            if not idx_segment:
                idx_data.append(idx)
                pass

            idx_segment = True				
        else:
            idx_segment = False


    # Pattern compare
    pattern_tmp = get_pattern(data_window, idx_data[0], idx_data[1])


    similarity_A = dtw(default_patterns[0], pattern_tmp)
    similarity_B = dtw(default_patterns[1], pattern_tmp)

    if similarity_A < similarity_B:
        patterns_A.append(pattern_tmp)
        dtw_A.append(similarity_A)
        dtw_AB.append(similarity_A)
    else:
        patterns_B.append(pattern_tmp)
        dtw_B.append(similarity_B)
        dtw_AB.append(similarity_B)
     
    
    start_idx += idx_data[1] - idx_data[0]
    data_window.clear()

    



figsize=(14, 6)

plt.figure(figsize=figsize)
plt.subplot(4, 1, 1)  # First, raw time series
plt.plot(default_series.ravel(), "b-")
# plt.plot(patterns[0], "b-", 'red')
# plt.hlines(sax.breakpoints_avg_ , default_series.min(), default_series.max(), 'gray', 'dashed', alpha=0.4)
# plt.vlines(idx_data, min(default_series), max(default_series), 'red', 'dashed', lw=1)
plt.title("Raw time series")

# plt.subplot(3, 1, 2)  # Then SAX
# plt.plot(series.ravel(), "b-", alpha=0.4)
# plt.plot(sax_dataset_inv[0].ravel(), 'b-')
# plt.hlines(sax.breakpoints_avg_ , 0, resample_size, 'gray', 'dashed', alpha=0.4)
# plt.vlines(idx_data, min_scaler, max_scaler, 'r', 'dashed', lw=1)
# plt.title("SAX, %d symbols" % n_sax_symbols)

# plt.subplot(3, 2, 3)
# plt.plot(default_patterns[0], "b-")
# plt.title("Pattern default")

# plt.subplot(3, 2, 4)
# plt.plot(pattern_tmp, "b-")
# plt.title("Pattern tmp")

plt.subplot(4, 1, 2)
plt.plot(dtw_A, "b-")
plt.title("dtw_A")

plt.subplot(4, 1, 3)
plt.plot(dtw_B, "b-")
plt.title("dtw_B")

plt.subplot(4, 1, 4)
plt.plot(dtw_AB, "b-")
plt.title("dtw_AB")


plt.tight_layout()

# plt.draw()
# plt.pause(1e-17)

plt.show()

    
