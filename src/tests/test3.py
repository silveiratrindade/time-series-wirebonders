import datetime
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import scipy.signal as sig
from tslearn.preprocessing import TimeSeriesScalerMinMax

import saxpy.sax as sx
from saxpy.znorm import znorm
from saxpy.paa import paa
# from saxpy.alphabet import cuts_for_asize

import src.packages.db.db_connect as db
import src.packages.utils.dataset as ds

# %matplotlib notebook

register_matplotlib_converters()

figsize=(9.8, 5)

axes = 'y_axis'
dataset_raw, ts, te = ds.get_dataset_from_db(start_min=54, start_sec=40, interval_sec=1)

dataset = dataset_raw[axes].values
timestamp = dataset_raw['timestampdata']

# series = pd.Series(dataset, index=timestamp)


# Resample
# series = series.resample('L').mean()
resample_size = 2000
paa_size = resample_size
alphabet_size = 5

sax_layers = sx.cuts_for_asize(alphabet_size + 2)
sax_layers = list(pd.Series(sax_layers))
sax_layers.pop(0)

paa_layer = []

for i in range(resample_size):
    paa_layer.append(i)

paa_rep = paa(znorm(dataset, 0.01), paa_size)


dataset = sig.resample(dataset, resample_size)

# Scale
# dataset = dataset + min(dataset)
# dataset = dataset/max(dataset)    

scaler = TimeSeriesScalerMinMax(min=min(sax_layers), max=max(sax_layers))
dataset = scaler.fit_transform(dataset)

series = pd.Series(dataset.ravel())

series2 = pd.Series(paa_rep.ravel())

chunck = sx.sax_by_chunking(series2, paa_size=paa_size, alphabet_size=alphabet_size)

print('')
print(chunck)

# Segmentation

pattern = 'e'
replace_pattern = ''

for i in range(len(pattern)):
    replace_pattern += '0'

idx_pattern = []

list_pattern = chunck.partition(pattern)

while list_pattern[2] != '':
        
    idx_pattern.append(len(list_pattern[0]))
    chunck = chunck.replace(pattern, replace_pattern, 1)
    list_pattern = chunck.partition(pattern)
    



rows = series.size

plt.figure(figsize=figsize)

plt.hlines(sax_layers, 0, resample_size, 'gray', 'dashed', alpha=0.4)
# plt.vlines(paa_layer, max(sax_layers), min(sax_layers), 'gray', 'dashed', alpha=0.4)

# sax_hlines = 1/(alphabet_size + 1)

# line = 0

# for l in range(alphabet_size):

#     plt.hlines(line, 0, resample_size, 'red', 'dashed', alpha=0.4)
#     line += sax_hlines

plt.plot(series)
plt.plot(idx_pattern, series[idx_pattern], 's', ms=5, mfc='red')


# for pa in idx_pattern:
#     plt.plot(pa, series[pa], 's', ms=5, mfc='red')
    # plt.hlines(0.5, 0, 1, 'red')

# plt.plot(series, 'o', ms=3, mfc='red')
plt.title(str(rows) + ' rows')

# plt.plot(series)

plt.show()




