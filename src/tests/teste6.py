import numpy as np
import pandas as pd
import scipy.signal as sig
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from tslearn.preprocessing import TimeSeriesResampler, TimeSeriesScalerMinMax
from tslearn.piecewise import PiecewiseAggregateApproximation, SymbolicAggregateApproximation
from tslearn.metrics import lb_envelope, dtw, soft_dtw, dtw_path

import src.packages.db.db_connect as db
import src.packages.utils.dataset as ds

# %matplotlib notebook
# %matplotlib inline
# %matplotlib widget

register_matplotlib_converters()

figsize=(8, 4)

# Utils
def get_pattern(dataset, idx_start, idx_end):
    pattern = []

    for idx in range(idx_start, idx_end):

        pattern.append(dataset[idx])

    return pattern

def get_segmentation(series_env, layers_pos, layer_select):
    
    idx_data = []
    
    segment = True

    for i in range(len(series_env)):

        if series_env[i] > layers_pos[layer_select]:
            # idx_data.append(i)
            if not segment:
                idx_data.append(i)
            segment = True
        else:
            segment = False
            
    return idx_data  

def print_one(figsize, series1, title):

    plt.figure(figsize=figsize)

    # plt.plot(dataset_timestamp, default_series, "b-")
    plt.plot(series1, "b-")
    plt.xlabel('Total de Pontos')
    plt.ylabel('Amplitude - Eixo Y')

#     plt.title("Dados Coletados - Vibração de 1 eixo de operação - %d pontos" % series.size)
    # plt.title(title % len(series1))
    plt.title(title)

    plt.show()
    
def print_two(figsize, series1, print_sec1, series_sec1, series2, print_sec2, series_sec2, title1, title2, \
                alpha, print_layers, series_layers, print_seg, series_seg):
    
    plt.figure(figsize=figsize)
    plt.subplot(2, 1, 1)  # First, raw time series
    plt.plot(series1, "b-", alpha=alpha)
    
    if print_sec1:
        plt.plot(series_sec1, "r-")
        
    if print_seg:
        plt.vlines(series_seg, min(series1), max(series1), 'red', 'dashed', lw=1)
    
    plt.xlabel('Pontos')
    plt.ylabel('Amplitude')

#     plt.title("Sem Filtros - %d pontos" % series_res.size)
    # plt.title(title1 % series1.size)
    plt.title(title1)

    plt.subplot(2, 1, 2)  # 
    plt.plot(series2, "b-", alpha=alpha)
    
    if print_sec2:
        plt.plot(series_sec2, "r-")
    
    plt.xlabel('Pontos')
    plt.ylabel('Amplitude')
    
    if print_layers:
        plt.hlines(series_layers, 0, series2.size, 'gray', 'dashed', alpha=alpha)
        
    if print_seg:
        plt.vlines(series_seg, min(series_layers), max(series_layers), 'red', 'dashed', lw=1)

#     plt.title("Filtro Valor Absoluto - %d pontos" % series_abs.size)
    plt.title(title2)

    plt.tight_layout()

    plt.show()    

def main():

    print(ds.get_dataset_start(qtd_rows=2))

    # Dataset
    axes = 'y_axis'
    timestamp = 'timestampdata'

    # precision = 100

    interval_sec = 70
    dataset_raw, ts, te = ds.get_dataset_from_db(start_year=2018, start_month=10, start_day=28, start_hour=21, \
        start_min=55, start_sec=13, interval_sec=interval_sec)

    dataset_axes = dataset_raw[axes]
    dataset_timestamp = dataset_raw[timestamp]

    # Series
    default_series = pd.Series(dataset_axes)
    series = pd.Series(dataset_axes)

    # print_one(figsize, default_series, "Dados Coletados - Vibração de 1 eixo de operação - "\
    #             + str(default_series.size) +" pontos")

    # Resample
    resample_size = 2000 * interval_sec
    default_series = pd.Series(TimeSeriesResampler(sz=resample_size).fit_transform(default_series).ravel())
    series_res = pd.Series(TimeSeriesResampler(sz=resample_size).fit_transform(series).ravel())

    # print_one(figsize, series_res, "Dados Reorganizados - Vibração de 1 eixo de operação - "\
    #             + str(default_series.size) +" pontos")

    # Absolute
    series_abs_op = series_res.abs()


    # print_two(figsize, series_res, False, 0, series_abs_op, False, 0, "Sem Filtros - "+ str(series_abs_op.size) +\
    #     " pontos", "Filtro Valor Absoluto", 1, False, 0, False, 0)

    # Envelope Operation
    env_low_op, env_up_op = lb_envelope(series_abs_op, radius=750)
    # env_low, env_up = lb_envelope(env_up, radius=15)
    series_env_op = pd.Series(env_up_op.ravel())

    # print_two(figsize, series_abs_op, False, 0, series_env_op, False, 0, "Filtro Valor Absoluto - "\
    #             + str(series_env_op.size) +" pontos", "Filtro Envelope", 1, False, 0, False, 0)

    #Layers Operation

    min_scaler = min(series_env_op)
    max_scaler = max(series_env_op)
    n_layers = 30
    n_segments = resample_size

    layer_dist = (max_scaler - min_scaler)/n_layers
    layer_tmp = min_scaler

    layers_pos_op = []

    for i in range(n_layers +1):    
        layers_pos_op.append(layer_tmp)
        layer_tmp += layer_dist


    layers_size_op = str(pd.Series(layers_pos_op).size)

    # print_two(figsize, series_abs_op, True, series_env_op, series_abs_op, True, series_env_op, \
    #     "Filtro Envelope - "+ str(series_env_op.size) +" pontos", "Divisão em camadas - "+ layers_size_op +\
    #     " camadas", 0.4, True, layers_pos_op, False, 0)

    idx_data_op = get_segmentation(series_env_op, layers_pos_op, 5)

    # print_two(figsize, series_res, False, 0, series_env_op, False, 0, "Segmentação das Operações - "\
    #             + str(series_env_op.size) +" pontos", "Divisão em camadas - "+layers_size_op+" camadas",\
    #             0.7, True, layers_pos_op, True, idx_data_op)


    # Operation Selection
    operationA = pd.Series(get_pattern(series_res, idx_data_op[0], idx_data_op[1]))

    # Absolute
    series_abs_sig = operationA.abs()

    # Envelope Signature
    env_low_sig, env_up_sig = lb_envelope(series_abs_sig, radius=25)
    # env_low, env_up = lb_envelope(env_up, radius=15)
    series_env_sig = pd.Series(env_up_sig.ravel())

    #Layers Signature

    min_scaler = min(series_env_sig)
    max_scaler = max(series_env_sig)
    n_layers = 30
    n_segments = pd.Series(operationA).size

    layer_dist = (max_scaler - min_scaler)/n_layers
    layer_tmp = min_scaler

    layers_pos_sig = []

    for i in range(n_layers +1):    
        layers_pos_sig.append(layer_tmp)
        layer_tmp += layer_dist

    layers_size_sig = str(pd.Series(layers_pos_op).size)
    # print_two(figsize, operationA, False, 0, series_abs_sig, True, series_env_sig, "Operation A - "\
    #             + str(series_env_sig.size) +" pontos", "Filtros Aplicados - "+ layers_size_sig +" camadas",\
    #             0.7, True, layers_pos_sig, False, 0)


    idx_data_sig = get_segmentation(series_env_sig, layers_pos_sig, 2)

    layers_size_sig = str(pd.Series(layers_pos_op).size)

    # print_two(figsize, operationA, False, 0, series_abs_sig, True, series_env_sig, "Operation A - "\
    #             + str(series_env_sig.size) +" pontos", "Divisão em camadas - "+ layers_size_sig +" camadas",\
    #             0.7, True, layers_pos_sig, True, idx_data_sig)

    op_segments = []

    seg_idx = 7

    for i in range(len(idx_data_sig)):
        
        op_segments.append(get_pattern(operationA, idx_data_sig[i -1], idx_data_sig[i]))   

    # print_one(figsize, op_segments[seg_idx], "Segment "+ str(seg_idx) + " - "+ str(len(op_segments[seg_idx])) +" Pontos ")


    # segments = ["a","b","c","a","c","a","b","c","e","b","h"]

    pattern_types = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]

    segments = op_segments.pop(0)

    patterns = []

    patterns.append([[pattern_types[0], 0]])

    for i in range(len(segments)):   

        similars = False 

        for j in range(len(patterns)):

            print(str(patterns[j][0][0]) + " - " + str(patterns[j][0][1]))

            segments_tmp = segments[patterns[j][0][1]]
            
            similarity = dtw(segments_tmp, segments[i])

            # if patterns[j][0][0] == segments[i]:

            if similarity > 0 and similarity < 10:        

                similars = True
                
        pattern_tmp = [pattern_types[i], i]       

        if pattern_tmp not in patterns and similars == True:

            patterns.append([pattern_tmp])
            
            
        

        for k in range(len(segments)):
            
            if k < i +1:
                continue

            print(str(i) + "," + str(segments[i]) + " -- " + str(k) + "," + str(segments[k]))  

            if pattern_types[i] == pattern_types[k] and len(patterns) > i:

                patterns[i].append([pattern_types[k], k])
            
        
        print(patterns)

        for l in range(len(patterns)):

            print(str(patterns[l][0][0]) + " - " + str(len(patterns[l])))

        # print(patterns[0])
        # print(patterns[0][0])
        # print(patterns[0][1])
        # print(patterns[0][0][1])





if __name__ == "__main__":
    main()