import numpy as np
import pandas as pd
import scipy.signal as sig
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from tslearn.preprocessing import TimeSeriesResampler, TimeSeriesScalerMinMax
from tslearn.piecewise import PiecewiseAggregateApproximation, SymbolicAggregateApproximation
from tslearn.metrics import lb_envelope, dtw, soft_dtw, dtw_path


# segments = ["a","b","c","a","c","a","b","c","e","b","h"]
# segments = [1,2,3,8],[1,2,100,5],[1,3,6,9],[2,60,7,8],[4,5,70,8],[2,3,5,6],[5,6,7,8],[1,30,5,7],[2,5,6,7]
segments = [1,2,3,8],[1,2,100,5],[1,3,6,9],[2,60,7,8]

pattern_types = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]

patterns = []
# patterns2 = []

# patterns.append([[pattern_types[0], 0]])

patterns.append([[segments[0], 0, pattern_types[0]]])

for i in range(len(segments)):   

    # similars = False

    for j in range(len(patterns)):

        # if j < i +1:
        #     continue  

        for k in range(len(patterns[j])):

            if i == patterns[j][k][1]:
                continue

            similarity = dtw(segments[i], patterns[j][k][0])

            if similarity > 10 and k != patterns[j][k][1]:

                patterns.append([[segments[i], i, pattern_types[i]]])


    for l in range(len(segments)):

        # similars = False
        
        if l < i +1:
            continue    

        # segments_tmp = segments[patterns[j][0][1]]
        
        similarity = dtw(segments[l], segments[i])

        if similarity > 0 and similarity < 10:        

            # similars = True

            patterns[i].append([segments[l], l, pattern_types[i]])

        # else:

        #     patterns.append([segments[k], k, similarity, pattern_types[i]])
        

    # for j in range(len(patterns)):

    #     if len(patterns) == 1:
    #         continue

    #     # print(str(patterns[j][0][0]) + " - " + str(patterns[j][0][1]))

    #     segments_tmp = segments[patterns[j][0][1]]
        
    #     similarity = dtw(segments_tmp, segments[i])

    #     # if patterns[j][0][0] == segments[i]:

    #     if similarity > 0 and similarity < 10:        

    #         similars = True

    #     # if patterns[j][0][0] == segments[i]:

    #     #     similars = True

    # if segments[i] not in patterns and similars == True:

    #     patterns.append([[segments[i], i]])

    # for k in range(len(segments)):
        
    #     if k < i +1:
    #         continue

    #     # print(str(i) + "," + segments[i] + " -- " + str(k) + "," + segments[k])  

    #     segments_tmp = segments[patterns[k][0][1]]
        
    #     similarity = dtw(segments_tmp, segments[i])

    #     if similarity > 0 and similarity < 10:        

    #         similars = True


    #     if segments[i] == segments[k] and len(patterns) > i:

    #         patterns[i].append([segments[k], k])
        
    
print(patterns)

for l in range(len(patterns)):

    print(str(patterns[l][0][0]) + " - " + str(len(patterns[l])))

# print(patterns[0])
print(patterns[0][0])
print(patterns[0][1])
# print(patterns[0][0][1])