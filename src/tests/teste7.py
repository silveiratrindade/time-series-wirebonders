import numpy as np
import pandas as pd
import scipy.signal as sig
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from tslearn.preprocessing import TimeSeriesResampler, TimeSeriesScalerMinMax
from tslearn.piecewise import PiecewiseAggregateApproximation, SymbolicAggregateApproximation
from tslearn.metrics import lb_envelope, dtw, soft_dtw, dtw_path

import src.packages.db.db_connect as db
import src.packages.utils.dataset as ds

# %matplotlib notebook
# %matplotlib inline

# %matplotlib widget

register_matplotlib_converters()

figsize=(8, 4)

# Utils
def get_pattern(dataset, idx_start, idx_end):
    
    pattern = []
    for idx in range(idx_start, idx_end):
        pattern.append(dataset[idx])
        
    return pattern

def get_layers(series, n_layers=30):    

    min_scaler = min(series)
    max_scaler = max(series)
    layer_dist = (max_scaler - min_scaler)/n_layers
    layer_tmp = min_scaler
    layers_pos = []
    for i in range(n_layers +1):    
        layers_pos.append(layer_tmp)
        layer_tmp += layer_dist
    layers_size = str(pd.Series(layers_pos).size)   
    
    return layers_pos, layers_size

def get_segmentation(series_env, layer_select, n_layers=30):
    
    idx_data = [] 
    layers_pos, layers_size = get_layers(series_env, n_layers)  
    segment = True
    for i in range(len(series_env)):
        if series_env[i] > layers_pos[layer_select]:
            if not segment:
                idx_data.append(i)
            segment = True
        else:
            segment = False
            
    return idx_data  

def print_one(figsize, series1, title, print_layers, layers, print_idx, idx):

    plt.figure(figsize=figsize)
    # plt.plot(dataset_timestamp, default_series, "b-")
    plt.plot(series1, "b-")
    plt.xlabel('Total de Pontos')
    plt.ylabel('Amplitude - Eixo Y')
#     plt.title("Dados Coletados - Vibração de 1 eixo de operação - %d pontos" % series.size)
    plt.title(title % len(series1))    
    if print_layers:
        plt.hlines(layers, 0, series1.size, 'gray', 'dashed', alpha=0.4)        
    if print_idx:
        plt.vlines(idx, min(series1), max(series1), 'red', 'dashed', lw=1)    
    plt.show()
    
def print_similarity(figsize, series1, series2, series3):
    
    similarity1 = dtw(series1, series2)
    similarity2 = dtw(series1, series3)
    similarity3 = dtw(series2, series3)
    plt.figure(figsize=figsize)
    # plt.plot(dataset_timestamp, default_series, "b-")
    plt.plot(series1, "b-", label="S1")
    plt.plot(series2, "red", label="S2", alpha=0.8)
    plt.plot(series3, "green", label="S3", alpha=0.8)
    plt.xlabel('Total de Pontos')
    plt.ylabel('Amplitude - Eixo Y')
    plt.legend(loc='lower right')
#     plt.title("Dados Coletados - Vibração de 1 eixo de operação - %d pontos" % series.size)
#     plt.title(title % similarity1)
    plt.title("Similaridade - (S1 <--> S2 = "+ str(round(similarity1, 2)) +") - (S1 <--> S3 = "+ str(round(similarity2, 2)) +") - (S2 <--> S3 = "+ str(round(similarity3, 2)) +")")
    plt.show()
    
def print_two(figsize, series1, print_sec1, series_sec1, series2, print_sec2, series_sec2, title1, title2, alpha, \
              print_layers, series_layers, print_seg, series_seg):
    
    plt.figure(figsize=figsize)
    plt.subplot(2, 1, 1)  # First, raw time series
    plt.plot(series1, "b-", alpha=alpha)    
    if print_sec1:
        plt.plot(series_sec1, "r-")        
    if print_seg:
        plt.vlines(series_seg, min(default_series), max(default_series), 'red', 'dashed', lw=1)    
    plt.xlabel('Pontos')
    plt.ylabel('Amplitude')
#     plt.title("Sem Filtros - %d pontos" % series_res.size)
    plt.title(title1 % series1.size)
    plt.subplot(2, 1, 2)  # 
    plt.plot(series2, "b-", alpha=alpha)    
    if print_sec2:
        plt.plot(series_sec2, "r-")    
    plt.xlabel('Pontos')
    plt.ylabel('Amplitude')    
    if print_layers:
        plt.hlines(series_layers, 0, series2.size, 'gray', 'dashed', alpha=alpha) 
    plt.title(title2)
    plt.tight_layout()
    plt.show()    
    
def get_dataset(axe_col, timestamp_col, interval_sec):
    
    dataset_raw, ts, te = ds.get_dataset_from_db(start_year=2018, start_month=10, start_day=28, start_hour=21, start_min=55, \
                                             start_sec=13, interval_sec=interval_sec)
    
    return dataset_raw[axe_col], dataset_raw[timestamp_col]

def get_series(interval_sec, is_resample, is_abs, is_envelope, radius=750, resample=2000):
    
    dataset_axes, dataset_timestamp = get_dataset('y_axis','timestampdata', interval_sec)    
    default_series = pd.Series(dataset_axes)
    series = pd.Series(dataset_axes)    
    if is_resample:        
        resample_size = resample * interval_sec
        default_series = pd.Series(TimeSeriesResampler(sz=resample_size).fit_transform(default_series).ravel())
        series = pd.Series(TimeSeriesResampler(sz=resample_size).fit_transform(series).ravel())        
    if is_abs:
        series = series.abs()        
    if is_envelope:
        # Envelope Operation
        env_low_op, env_up_op = lb_envelope(series, radius=radius)
        # env_low, env_up = lb_envelope(env_up, radius=15)
        series = pd.Series(env_up_op.ravel())    
        
    return default_series, series

def group_segments(operation, envelope_rad = 25):
    
    op_segments = []    
    default_op = operation    
    operation = operation.abs()    
    env_low_sig, env_up_sig = lb_envelope(operation, radius=envelope_rad)    
    series_sig = pd.Series(env_up_sig.ravel())    
    idx_data = get_segmentation(series_sig, 2)    
    while len(idx_data) > 262:        
        idx_data.pop(0)
    for i in range(len(idx_data)):
        op_segments.append(get_pattern(default_op, idx_data[i -1], idx_data[i]))
    op_segments.pop(0)
    
    return op_segments, env_up_sig, idx_data

def compare_similarity_1_op(arr, idx_similarity):
    
    arr_sim = []    
    pat = []    
    pat.append(arr[0])    
    no_pat = []
    for i in range(1,len(arr)):
        similarity = dtw(arr[0], arr[i])        
        arr_sim.append(similarity)        
        if similarity < idx_similarity:
            pat.append(arr[i])
        else:
            no_pat.append(arr[i])
            
    return pat, no_pat

def compare_similarity_2_op(pattern_compare, op_segment, idx_similarity):

    pat = []    
    no_pat = []    
    for i in range(len(op_segment)):
        similarity = dtw(pattern_compare, op_segment[i])
        if similarity < idx_similarity:
            pat.append(op_segment[i])
        else:
            no_pat.append(op_segment[i])  

    return pat, no_pat

def group_by_similarity(arr_pat, idx_similarity):
    
    default_arr_pat = arr_pat
    sequence = []
    idx_pat_i = []
    idx_pat_j = []
    while len(arr_pat) > 0:
        pattern_tmp, arr_pat = compare_similarity_1_op(arr_pat, idx_similarity)
        sequence.append(pattern_tmp)     
    for i in range(len(sequence)):
        idx_pat_j = []
        for j in sequence[i]:
            idx_pat_j.append(default_arr_pat.index(j))
        idx_pat_i.append(idx_pat_j)
        
    return sequence, idx_pat_i      

def group_by_similarity_default(sequence_default, op_segment, idx_similarity):
    
    tmp_op_segment = op_segment
    sequence = []
    idx_pat_i = []
    idx_pat_j = []

#     while len(op_segment) > 0:        
    for i in range(len(sequence_default)):             
        pattern_tmp, op_segment = compare_similarity_2_op(sequence_default[i][0], op_segment, idx_similarity)
        sequence.append(pattern_tmp)  
    for i in range(len(sequence)):
        idx_pat_j = []
        for j in sequence[i]:
            idx_pat_j.append(tmp_op_segment.index(j))
        idx_pat_i.append(idx_pat_j)

    return sequence, idx_pat_i 

def config_select(option):
    
    envelope_radius_op = None
    envelope_radius_sig = None
    resample_size = None
    if option == 1:    
        #config1
        envelope_radius_op = 750
        envelope_radius_sig = 25
        resample_size = 2000    
    elif option == 2:        
        #config2
        envelope_radius_op = 6000
        envelope_radius_sig = 300
        resample_size = 20000        
    else:        
        print("")
        print("Config não existe! Escolha 1 ou 2")
        print("")
    
    return envelope_radius_op, envelope_radius_sig, resample_size

def print_sequence_groups(op_segments, sequence):
    
    print("")
    print("----------")
    print(str("Total signatures: " + str(len(op_segments))))
    print("----------")
    print("Group by Similarity:")
    print("")
    for s in range(0, len(sequence)):
        print(str(s) + " - " + str(len(sequence[s])))
        
        
def calc_op_quality(op_segments1, op_segments2, idx_similarity):
    
    pat = []
    no_pat = []    
    for i in range(1, len(op_segments1)):        
        similarity = dtw(op_segments1[i], op_segments2[i])        
        if similarity < idx_similarity:            
            pat.append(op_segments1[i])            
        else:            
            no_pat.append(op_segments1[i])            
    sim = len(pat)    
    no_sim = len(no_pat)    
    quality = (sim * 100)/len(op_segments1)
            
    return quality, pat

def op_cfg_plot(cfg):
    
    if cfg == 1:    
        operation = operation1
        op_segments = op_segments1
        idx_data = idx_data1
    elif cfg == 2:    
        operation = operation2
        op_segments = op_segments2
        idx_data = idx_data2
    elif cfg == 3:    
        operation = operation3
        op_segments = op_segments3
        idx_data = idx_data3

    return operation, op_segments, idx_data

def main():

    #Amostra em segundos
    interval_sec = 70

    envelope_radius_op, envelope_radius_sig, resample_size = config_select(1)

    #Series
    default_series, series = get_series(interval_sec, True, True, True, envelope_radius_op, resample_size)

    # #Index Operation
    idx_data_op = get_segmentation(series, 5)

    # #Operations
    operation1 = pd.Series(get_pattern(default_series, idx_data_op[0], idx_data_op[1]))
    operation2 = pd.Series(get_pattern(default_series, idx_data_op[1], idx_data_op[2]))
    operation3 = pd.Series(get_pattern(default_series, idx_data_op[2], idx_data_op[3]))

    # #Segments
    op_segments1, env_up_sig1, idx_data1 = group_segments(operation1, envelope_radius_sig)
    op_segments2, env_up_sig2, idx_data2 = group_segments(operation2, envelope_radius_sig)
    op_segments3, env_up_sig3, idx_data3 = group_segments(operation3, envelope_radius_sig)

    print(str(len(op_segments1)) + " - " + str(len(op_segments2)) + " - " + str(len(op_segments3)))

    # print(str(len(op_segments1)))
    print("OK")

    # Sequenciar/Agrupar

    similarity = 5

    sequence1, idx_sequence1 = group_by_similarity(op_segments1, similarity)
    # sequence2, idx_sequence2 = group_by_similarity(op_segments2, similarity)
    sequence3, idx_sequence3 = group_by_similarity(op_segments3, similarity)
    print("---------------------")
    sequence2, idx_sequence2 = group_by_similarity_default(sequence1, op_segments2, similarity)
    # sequence3, idx_sequence3 = group_by_similarity_default(sequence1, op_segments3, similarity)


    print_sequence_groups(sequence1, idx_sequence1)
    print_sequence_groups(sequence2, idx_sequence2)
    print_sequence_groups(sequence3, idx_sequence3)
    






if __name__ == "__main__":
    main()