import numpy as np
import pandas as pd
import scipy.signal as sig
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from tslearn.preprocessing import TimeSeriesResampler, TimeSeriesScalerMinMax
from tslearn.piecewise import PiecewiseAggregateApproximation, SymbolicAggregateApproximation
from tslearn.metrics import lb_envelope, dtw, soft_dtw, dtw_path



# segments = ["a","b","c","a","c","a","b","c","e","b","h"]
segments = [1,2,3,8],[1,2,100,5],[1,3,6,9],[2,60,7,8],[4,5,70,8],[2,3,5,6],[5,6,7,8],[1,30,5,7],[2,5,6,7]
# segments = [1,2,3,8],[1,2,100,5],[1,3,6,9],[2,60,7,8]

pattern_types = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]

