import numpy as np
import pandas as pd
import scipy.signal as sig
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from tslearn.preprocessing import TimeSeriesResampler, TimeSeriesScalerMinMax
from tslearn.piecewise import PiecewiseAggregateApproximation, SymbolicAggregateApproximation
from tslearn.metrics import lb_envelope, dtw, soft_dtw, dtw_path



# segments = ["a","b","c","a","c","a","b","c","e","b","h"]
segments = [1,2,3,8],[1,2,100,5],[1,3,6,9],[2,60,7,8],[4,5,70,8],[2,3,5,6],[5,6,7,8],[1,30,5,7],[2,5,6,7]
# segments = [1,2,3,8],[1,2,100,5],[1,3,6,9],[2,60,7,8]

pattern_types = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]

patterns = []
not_patterns = []
# patterns2 = []

patterns.append([pattern_types[0], 0])

# patterns.append([[segments[0], 0, pattern_types[0]]])

for i in range(len(segments)): 
    
#     print("i - " + str(i) +" - " +str(segments[i]))

#     for j in range(len(patterns)):

#         print("j - " + str(i) + " - " + str(segments[i]))

#         for k in range(len(patterns[j])):

#             if i == patterns[j][k][1]:
#                 continue

#             similarity = dtw(segments[i], patterns[j][k][0])

#             if similarity > 10 and k != patterns[j][k][1]:

#                 patterns.append([[segments[i], i, pattern_types[i]]])
    # not_patterns = []

    if i > 0:
                
        patterns.append([pattern_types[i]])  

    for j in range(len(segments)):

        # similars = False
        
        if j < i +1:
            continue    
            
#         print("l - " + str(l) +" - " +str(segments[l]))

        # segments_tmp = segments[patterns[j][0][1]]        
        
        similarity = dtw(segments[j], segments[i])

        if similarity > 0 and similarity < 10:      

            # similars = True

            # patterns[i].append([segments[j], j, pattern_types[i]])
            patterns[i].append(j)
            
        else:

            if segments[j] not in not_patterns:
            
                not_patterns.append(segments[j])
            



#     for k in range(len(not_patterns)):
        
#         for l in range(len(patterns)):
            
#             similarity = dtw(not_patterns[k][0][0], patterns[l][0][0])
            
#             print(not_patterns[k][0][0])
#             print(patterns[l][0][0])
            
#             if similarity > 10:
                
#                 patterns.append([[not_patterns[k][0][0], k, pattern_types[k]]])
            
    
print(segments)
print("")

for p in range(len(patterns)):

    print(patterns[p])

print("")

for n in range(len(not_patterns)):

    print(not_patterns[n])
