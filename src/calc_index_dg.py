import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt

from tslearn.preprocessing import TimeSeriesResampler, TimeSeriesScalerMinMax
from tslearn.piecewise import PiecewiseAggregateApproximation, SymbolicAggregateApproximation
from tslearn.metrics import lb_envelope

import src.packages.db.db_connect as db
import src.packages.utils.dataset as ds

# Pandas dependency for matplotlib. Don't remove!
register_matplotlib_converters()

def Preprocessing(dataset_axes, dataset_timestamp, interval_sec=2, absolute=False, resample=False, n_segments=2000, \
    envelope=True, n_sax_layers=20, first_layer_adjust=.2):

    # Transform to Series
    default_series = pd.Series(dataset_axes)
    series = pd.Series(dataset_axes, index=dataset_timestamp)

    # Transform to Absolute
    if absolute:
        series = series.abs()

    # Resample
    if resample:
        resample_size = n_segments * interval_sec
        default_series = pd.Series(TimeSeriesResampler(sz=resample_size).fit_transform(default_series).ravel())
        series = pd.Series(TimeSeriesResampler(sz=resample_size).fit_transform(series).ravel())

    # Envelope
    if envelope:
        env_low, env_up = lb_envelope(series, radius=35)
        series = pd.Series(env_up.ravel())

    # Scale to Sax
    sax = SymbolicAggregateApproximation(n_segments=n_segments, alphabet_size_avg=n_sax_layers)

    min_scaler = min(sax.breakpoints_avg_middle_) + first_layer_adjust
    max_scaler = max(sax.breakpoints_avg_middle_)

    scaler = TimeSeriesScalerMinMax(min=min_scaler, max=max_scaler)
    series = scaler.fit_transform(series)

    series = pd.Series(sax.inverse_transform(sax.fit_transform(series))[0].ravel())

    return series, default_series, min_scaler, max_scaler, sax.breakpoints_avg_middle_, sax.breakpoints_avg_, n_sax_layers, n_segments

def Segmentation(series, sax_breakpoints_avg_middle):

    sax_list = list(series)
    segmentation = []

    segment = True

    for i in range(len(sax_list)):
            
        if sax_list[i] != sax_breakpoints_avg_middle:
            # segmentation.append(i)	
            if not segment:
                segmentation.append(i)
                pass

            segment = True				
        else:
            segment = False

    return segmentation

def Plot(series, default_series, segmentation, n_sax_layers, sax_breakpoint_avg, n_segments, min_scaler, max_scaler):

    figsize=(14, 6)

    plt.figure(figsize=figsize)
    plt.subplot(2, 1, 1)  # First, raw time series
    plt.plot(default_series.ravel(), "b-")
    plt.vlines(segmentation, min(default_series), max(default_series), 'red', 'dashed', lw=1)

    plt.title("Raw time series")

    plt.subplot(2, 1, 2)  # Then SAX
    # plt.plot(series.ravel(), "b-", alpha=0.4)
    plt.plot(series, 'b-')
    plt.hlines(sax_breakpoint_avg, 0, n_segments, 'gray', 'dashed', alpha=0.4)
    plt.vlines(segmentation, min_scaler, max_scaler, 'r', 'dashed', lw=1)

    plt.title("SAX, %d symbols" % n_sax_layers)

    plt.tight_layout()

    plt.show()


def main():

    axes = 'y_axis'
    timestamp = 'timestampdata'
    # interval_sec = 5
    precision = 5

    # Get Dataset from DB
    dataset_raw, ts, te = ds.get_dataset_from_db(start_min=54, start_sec=41, interval_sec=precision)

    # dataset_raw = ds.get_dataset_from_csv(r'C:\_Trindade\Git\time-series-csv\data\csv\split_1M\output_1.csv')

    dataset_axes = dataset_raw[axes].values
    dataset_timestamp = dataset_raw[timestamp]


    series, default_series, min_scaler, max_scaler, sax_breakpoints_avg_middle, sax_breakpoints_avg, n_sax_layers, n_segments = Preprocessing(dataset_axes, dataset_timestamp)

    segmentation = Segmentation(series, sax_breakpoints_avg_middle[0])

    Plot(series,default_series, segmentation, n_sax_layers, sax_breakpoints_avg, n_segments, min_scaler, max_scaler)


    


if __name__ == "__main__":
    main()